#!/usr/bin/env bash

# Set Kubernetes cluster entry
KUBERNETES_CLUSTER_CERTIFICATE_AUTHORITY=KUBERNETES_CLUSTER_CERTIFICATE_AUTHORITY_${BITBUCKET_BRANCH}
echo ${!KUBERNETES_CLUSTER_CERTIFICATE_AUTHORITY} | base64 --decode > ./kubernetes-cluster-certificate-authority
KUBERNETES_CLUSTER_SERVER=KUBERNETES_CLUSTER_SERVER_${BITBUCKET_BRANCH}
kubectl config set-cluster default-cluster --server=${!KUBERNETES_CLUSTER_SERVER} --certificate-authority=./kubernetes-cluster-certificate-authority
# Set Kubernetes user entry
KUBERNETES_USER_CLIENT_CERTIFICATE=KUBERNETES_USER_CLIENT_CERTIFICATE_${BITBUCKET_BRANCH}
echo ${!KUBERNETES_USER_CLIENT_CERTIFICATE} | base64 --decode > ./kubernetes-user-client-certificate
KUBERNETES_USER_CLIENT_KEY=KUBERNETES_USER_CLIENT_KEY_${BITBUCKET_BRANCH}
echo ${!KUBERNETES_USER_CLIENT_KEY} | base64 --decode > ./kubernetes-user-client-key
KUBERNETES_USER_TOKEN=KUBERNETES_USER_TOKEN_${BITBUCKET_BRANCH}
kubectl config set-credentials default-user --client-certificate=./kubernetes-user-client-certificate --client-key=./kubernetes-user-client-key --token=${!KUBERNETES_USER_TOKEN}
# Set Kubernetes context entry
kubectl config set-context default-cluster --cluster=default-cluster --user=default-user
# Set the current-context
kubectl config use-context default-cluster

# Make sure to delete before creating secret due to deployment fail
kubectl delete secret regcred --ignore-not-found=true
kubectl create secret docker-registry --docker-password=${DOCKER_PASSWORD_DEPLOY} --docker-username=${DOCKER_USERNAME_DEPLOY} --docker-server=${DOCKER_REGISTRY} --docker-email=" " regcred
kubectl delete secret ssl --ignore-not-found=true
echo ${SSL_CERTIFICATE} | base64 --decode > ./ssl-certificate
echo ${SSL_KEY} | base64 --decode > ./ssl-key
kubectl create secret generic ssl --from-file=certificate=./ssl-certificate --from-file=key=./ssl-key
kubectl delete secret allgeier-appserver --ignore-not-found=true
ALLGEIER_APPSERVER_TOKEN=ALLGEIER_APPSERVER_TOKEN_${BITBUCKET_BRANCH}
ALLGEIER_APPSERVER_URL=ALLGEIER_APPSERVER_URL_${BITBUCKET_BRANCH}
kubectl create secret generic allgeier-appserver --from-literal=token=${!ALLGEIER_APPSERVER_TOKEN} --from-literal=url=${!ALLGEIER_APPSERVER_URL}
kubectl delete secret sendgrid-api --ignore-not-found=true
kubectl create secret generic sendgrid-api --from-literal=key=${SENDGRID_API_KEY} --from-literal=url=${SENDGRID_API_URL}
# Deploy resource to Kubernetes cluster
ENVIRONMENT=${BITBUCKET_BRANCH}
if [ "${ENVIRONMENT}" = master ]; then
    ENVIRONMENT=production
fi
cd /
make DOCKER_TAG=${BITBUCKET_COMMIT} DEPLOYMENT_ENVIRONMENT=${ENVIRONMENT} ${KUBERNETES_RESOURCE}
