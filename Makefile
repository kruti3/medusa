# Build the node image and deploy resource api to Kubernetes cluster.
#
# Usage:
# 	make [deploy|build|init|secret|clean]

TAG := $(shell date +%s)
REGISTRY := atyoucastaejobs.azurecr.io
IMAGE := $(REGISTRY)/node
BUILDENV_TAG := 1.0.1
BUILDENV_IMAGE := $(REGISTRY)/buildenv
RESOURCE := api

all: deploy

build:
ifndef ACR_ID_FILE
	$(error Please make sure ACR_ID_FILE is defined via '. setEnv.sh' and the ID file is properly set to chmod 600)
endif
ifndef ACR_PW_FILE
	$(error Please make sure ACR_PW_FILE is defined via '. setEnv.sh' and the password file is properly set to chmod 600)
endif
	cat $(ACR_PW_FILE) | docker login --username `cat $(ACR_ID_FILE)` --password-stdin $(REGISTRY)
	docker build -t $(IMAGE):$(TAG) -t $(IMAGE) .
	docker logout $(REGISTRY)

init:
	kubectl delete clusterrolebinding minikube-aejobs --ignore-not-found=true
	kubectl create clusterrolebinding minikube-aejobs --clusterrole=cluster-admin --serviceaccount=default:default

secret:
ifndef ACR_ID_FILE
	$(error Please make sure ACR_ID_FILE is defined via '. setEnv.sh' and the ID file is properly set to chmod 600)
endif
ifndef ACR_PW_FILE
	$(error Please make sure ACR_PW_FILE is defined via '. setEnv.sh' and the password file is properly set to chmod 600)
endif
	kubectl delete secret regcred --ignore-not-found=true
	kubectl create secret docker-registry --docker-password=`cat $(ACR_PW_FILE)` --docker-username=`cat $(ACR_ID_FILE)` --docker-server=$(REGISTRY) --docker-email=" " regcred
	kubectl delete secret ssl --ignore-not-found=true
	kubectl create secret generic ssl --from-literal=certificate=certificate --from-literal=key=key
	kubectl delete secret allgeier-appserver --ignore-not-found=true
	kubectl create secret generic allgeier-appserver --from-literal=token=token --from-literal=url=url
	kubectl delete secret sendgrid-api --ignore-not-found=true
	kubectl create secret generic sendgrid-api --from-literal=key=key --from-literal=url=url

deploy: build init secret
	kubectl delete job $(RESOURCE)-deploy --ignore-not-found=true
	kubectl run $(RESOURCE)-deploy --image=$(BUILDENV_IMAGE):$(BUILDENV_TAG) --restart=OnFailure --overrides='{ "apiVersion": "batch/v1", "spec": { "template": { "spec": { "imagePullSecrets": [ { "name": "regcred" } ] } } } }' --env="DOCKER_TAG=$(TAG)" --env="DEPLOYMENT_ENVIRONMENT=development" -- make $(RESOURCE)

clean: init secret
	kubectl delete po $(RESOURCE)-clean --ignore-not-found=true
	kubectl run $(RESOURCE)-clean --image=$(BUILDENV_IMAGE):$(BUILDENV_TAG) --restart=Never --overrides='{ "apiVersion": "v1", "spec": { "imagePullSecrets": [ { "name": "regcred" } ] } }' -- make $(RESOURCE).clean

	while kubectl get po | grep $(RESOURCE) | grep -v $(RESOURCE)-clean | grep -v $(RESOURCE)-deploy | grep Running; do sleep 1; done
	while kubectl get po | grep $(RESOURCE) | grep -v $(RESOURCE)-clean | grep -v $(RESOURCE)-deploy | grep Terminating; do sleep 1; done
	for i in `docker images -a | grep $(IMAGE) | grep -v $(IMAGE)-base | awk '{print $$3}'`; do docker rmi -f $$i; done

	kubectl delete job $(RESOURCE)-deploy --ignore-not-found=true
	kubectl delete secret regcred --ignore-not-found=true
	kubectl delete secret ssl --ignore-not-found=true
	kubectl delete secret allgeier-appserver --ignore-not-found=true
	kubectl delete secret sendgrid-api --ignore-not-found=true
