openapi: 3.0.0
info:
  title: AEJOBS API
  version: 1.0.1

paths:
  /users:
    get:
      responses:
        200:
          description: A JSON of user formData
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/formData'
    post:
      requestBody:
        content:
          application/json:
            schema:
                $ref: '#/components/schemas/formData'
      responses:
        201:
          description: Created

components:
  schemas:
    applicant:
      type: object
      description: Basic or minimal form data for a valid set of user data (Step 1-5 in form)
      properties:
        external_key:
          type: string
        age:
          type: integer
          minimum: 20
          maximum: 45
          description: Age of the applicant, limited to a range from 20 to 45.
        first_name:
          type: string
          minLength: 2
          description: The first name of the applicant, minimum string length is 2 characters long.
        last_name:
          type: string
          minLength: 2
          description: The last name of the applicant, minimum string length is 2 characters long.
        email:
          type: string
          description: The applicant's email address to contact, send invitation or opt-in keys to.
        phone:
          type: string
          minLength: 6
          description: The applicant's phone number for a direct contact
        country:
          type: string
          minLength: 2
          description: Optional country origin of the applicant, minimum string length is 2 characters
      required:
        - age
        - first_name
        - last_name
        - email
    formData:
      type: object
      description: Object handled by vuejs' local state management during form processing and user interaction.
      properties:
        applicant:
          $ref: '#/components/schemas/applicant'
        coding_languages:
          type: array
          description: List of strings the applicant/user enters into the form field, treated as tags.
          items:
            type: string
            description: A tag representing a coding language like c++, c#, python, ruby etc.
        final_agreement:
          type: string
          description: At the and of the form (Steps 6-8) the user should accept all data and privacy terms. String should contain 'accepted'.
        frameworks:
          type: array
          description: List of strings the applicant/user enters into the form field, treated as tags.
          items:
            type: string
            description: A tag representing a framework like vuejs, nuxt, laravel, symfony etc.
        job_selection:
          type: array
          description: Multichoice, selectable list of strings the applicant's own/personal job preferences refers to.
          items:
            type: string
            enum: [administrator , architect , business_analyst , developer , project_manager , supporter , tester , test_manager]
            description: Predefined list of job branches
        mutual_respect:
          type: boolean
          description: Applicant achieves the step to submit the first form data.
        platform_comfort_segment_1:
          type: array
          description: Multichoice, selectable list of strings of common frameworks.
          items:
            type: string
            enum: [framework_one , framework_two , framework_three , framework_four , framework_five , framework_six]
            description: A common framework.
        platform_comfort_segment_2:
          type: array
          description: Multichoice, selectable list of strings of common frameworks.
          items:
            type: string
            enum: [framework_one , framework_two , framework_three , framework_four , framework_five , framework_six]
            description: A common framework.
        proficiency:
          type: object
          description: How the applicant rates his/her skills on certain topics.
          properties:
            english_language:
              type: string
              enum: [excellent , very_good , good , average , poor]
              description: English language skills.
            german_language:
              type: string
              enum: [excellent , very_good , good , average , poor]
              description: German language skills.
            travel:
              type: string
              enum: [excellent , very_good , good , average , poor]
              description: Business traveling preferences (likes to).
          required:
            - english_language
            - german_language
        profiles:
          type: object
          description: Optional URLs to certain profiles of the applicant.
          properties:
            github:
              type: string
              format: uri
              description: Link to a github repository or account.
            stackoverflow:
              type: string
              format: uri
              description: Link to stackoverflow thread, account or category.
            linkedin:
              type: string
              format: uri
              description: Link to applicant's LinkedIn profile.
            xing:
              type: string
              format: uri
              description: Link to applicant's Xing profile.
            otherService:
              type: string
              format: uri
              description: Link to applicant's website or other.
            portfolio:
              type: string
              format: uri
              description: Link to applicant's portfolio document, website or other.
        work_type:
          type: array
          description: Multichoice, selectable list of strings of job areas.
          items:
            type: string
            enum: [long_term_gig , freelance , remote_work , big_company , start_up , looking_right_now , gain_experience , make_money , big_city , leading_a_team]
            description: Predefined list of job areas.
      required:
        - applicant
        - final_agreement
        - mutual_respect
