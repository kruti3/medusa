const _ = require('lodash')
const config = require('../../config/config')
//const enums = require('../../../enums')
const validation = require('../../app/components/validator/store/validationFunctions')
const validator = require('../../app/components/validator/store/validators')

/**
 * Mark these fields as "cant be there", i.e. id, password when updating, createdAt etc.
 * @return {*}
 */
function dontHave() {
    return {
        assert(val) {
            return ( val === undefined )
        },
        message: 'alert.isExist'
    }
}

const sameValidations = {
    user: {
        enabled: dontHave(),
        id: dontHave(),
        confirmed: dontHave()
    }
}

const password = {
    assert: 'isLength',
    args: {
        min: config.businessLogic.password.minLength,
        max: config.businessLogic.password.maxLength
    },
    message: `alert.isLength_${config.businessLogic.password.minLength}_${config.businessLogic.password.maxLength}`,
    required: true
}

const validations = {
    cockpit_user: {
        register: {
            form_data: {
                // on register external_key must be 0
                external_key: {
                    required: true,
                    assert: 'isInt',
                    args: [ { max: 0 } ],
                    message: 'alert.isInt'
                },
                applicant: {
                    email: {
                        required: true,
                        assert: 'isEmail',
                        message: 'alert.isEmail'
                    },
                    first_name: {
                        assert: 'isLength',
                        required: true,
                        args: [ { min: 1, max: 255 } ],
                        message: 'alert.isString'
                    },
                    last_name: {
                        assert: 'isLength',
                        required: true,
                        args: [ { min: 1, max: 255 } ],
                        message: 'alert.isString'
                    }
                }
            }
        },
        update: {
            form_data: {
                // on update external_key must be greater than 0
                //external_key: {
                //    required: true,
                //    assert: 'isInt',
                //    args: [ { min: 1 } ],
                //    message: 'alert.isInt'
                //},
                applicant: {
                    email: {
                        required: true,
                        assert: 'isEmail',
                        message: 'alert.isEmail'
                    },
                    first_name: {
                        assert: validation.requiredString(),
                        required: true,
                        message: 'alert.isString'
                    },
                    last_name: {
                        assert: validation.requiredString(),
                        required: true,
                        message: 'alert.isString'
                    }
                }
            }
        }
    },
    /*

    applicant: {
      age: '',
      first_question: '',
      first_name: '',
      last_name: '',
      dob: '',
      email: '',
      phone: '',
      residence: '',
      message: '',
      nationality: {}
    },

     */
    hr4you_user: {
        register: {
            form_data: {
                api: {
                    short: {
                        assert: 'isLength',
                        required: true,
                        args: [{ min: 1, max: 255 }],
                        message: 'alert.isString'
                    },
                },
                applicant: {
                    email: {
                        required: true,
                        assert: 'isEmail',
                        message: 'alert.isEmail'
                    },
                    first_name: {
                        assert: 'isLength',
                        required: true,
                        args: [{ min: 1, max: 255 }],
                        message: 'alert.isString'
                    },
                    last_name: {
                        assert: 'isLength',
                        required: true,
                        args: [{ min: 1, max: 255 }],
                        message: 'alert.isString'
                    },
                    /*
                    residence: {
                        assert: 'isLength',
                        required: true,
                        args: [{ min: 1, max: 255 }],
                        message: 'alert.isString'
                    },
                    dob: {
                        assert: validator.isDateWithFormat,
                        required: true,
                        args: [{format: "DD.MM.YYYY" }],
                        message: 'alert.isDate'
                    },
                    */
                }
            }
        }
    }
}

/**
 * Add required:true to selected fields
 * @param {!Object} _obj Object with fields
 * @param {Array.<!string>} fields These fields will have required:true
 * @return {*}
 */
/*function addRequired(_obj, fields = []) {
    const obj = _.cloneDeep(_obj)
    fields = _.castArray(fields)
    fields.forEach(field => {
        obj[ field ] = _.merge({ required: true }, obj[ field ])
    })

    return obj
}*/

module.exports = {
    cockpit_user: {
        register: validations.cockpit_user.register,
        update: validations.cockpit_user.update
    },
    hr4you_user: {
        register: validations.hr4you_user.register
    }
}
