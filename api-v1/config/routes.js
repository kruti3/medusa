const _ = require('lodash')
const act = require('../app/components/helperTools')

const cockpit = require('../app/routes/cockpit')
const hr4you  = require('../app/routes/hr4you')

const config = require('config')
const cors = require('cors')
const helperLog = require('../app/components/helperLog')
const helperTranslate = require('../app/components/helperTranslate')
const helper = require('../app/middleware/helper')
//const passport = require('passport')
//const compose = require('compose-middleware').compose

//const bearer = passport.authenticate('bearer', { session: false })
//const transformMeToUserId = compose(bearer, helper.transformMeToUserId)

module.exports = function (app) {

    // set a request-id
    app.use('/*', (req, res, next) => {
        if (!req.get('X-req-id')) {
            req.apiReqId = act.generateRandomAlphanumeric(30)
        } else {
            req.apiReqId = req.get('X-req-id')
        }
        if (config.params.fullReqLog === true) {
            helperLog.info({
                req,
                apiReqId: req.apiReqId
            }, 'Request accepted')
        }
        res.setHeader('X-Medusa-Env', process.env.NODE_ENV)
        return next()
    })

    // tell cors to use req-id
    app.use(cors({
        exposedHeaders: ['link', 'X-req-id'],
    }))

    // default response
    app.get('/', (req, res, next) => { // eslint-disable-line no-unused-vars
        res.json({
            apiReqId: req.apiReqId,
            api: 'medusa-api-v1',
        })
    })

    // simple "health" check
    app.get('/_health/:anything', (req, res) => {
        res.json({ health: 'check', apiReqId: req.apiReqId })
    })

    // default definitions
    app.use(cors())
    app.use('/*', helper.createPaginationObject)

    // simple API and users
    //app.use('/api/v1', api)
    if (config.allgeier.cockpit.enabled === true) {
        app.use('/api/v1/cp/user', cockpit)
    }
    if (!config.allgeier.hr4you.enabled === true) {
        app.use('/api/v1/hr/user', hr4you)
    }

    // Default error handler
    app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
        if(req.apiReqId) {
            res.setHeader('X-req-id', req.apiReqId)
        }
        const status = err.status || 500

        helperLog.error({ err, req, res, apiReqId: req.apiReqId }, 'Error handler at the end of app')

        res.status(status)
        if (config.params.fullErrors === false) {
            return res.json({ apiReqId: req.apiReqId })
        }

        const errMessage = _.get(err, 'message')
        let errMessageObj = {}
        try {
            const tmp = JSON.parse(errMessage)
            errMessageObj = {
                message: helperTranslate.translate(`errors.${_.get(tmp, 'localizationKey', 'default')}`, req.i18n.locale, _.get(tmp, 'replacements', [])),
                origMessage: _.get(tmp, 'origMessage'),
            }
        } catch (e) {
            errMessageObj = {
                message: errMessage,
                origMessage: null,
            }

            /* UNUSED from user login failure
            if (_.isObject(errMessage)) {
                if (errMessage.email) {
                    errMessageObj = {
                        message: helperTranslate.translate('errors.user.email', req.i18n.locale),
                        origMessage: errMessage,
                    }
                } else if (errMessage.password) {
                    errMessageObj = {
                        message: helperTranslate.translate('errors.user.password', req.i18n.locale, [
                            config.businessLogic.password.minLength,
                            config.businessLogic.password.maxLength,
                        ]),
                        origMessage: errMessage,
                    }
                }
            }*/
        }

        // populate errMessages
        return res.json({
            apiReqId: req.apiReqId,
            errMessage: _.get(errMessageObj, 'message'),
            errStack: _.get(err, 'stack'),
            errCode: _.get(err, 'errorCode', null),
        })
    })

    // Default middleware, that ends everything - either parse return value or send 404 if nothing was found
    app.use((req, res, next) => { // eslint-disable-line no-unused-vars
        res.setHeader('X-req-id', req.apiReqId)

        if (res.out) {
            if (config.params.fullReqLog === true) {
                helperLog.info({ req, res, apiReqId: req.apiReqId }, 'Standard output')
            }
            return res.json(res.out)
        }

        res.status(404)
        helperLog.info({ req, res, apiReqId: req.apiReqId }, 'Standard output 404')
        return res.json({ error: 'Not Found' })
    })
}
