module.exports = {
    log: {
        pino: {
            init: {
                prettyPrint: false,
            },
        },
    },
    server: {
        ssl: true,
        keyfile: '/etc/ssl-volume/key',
        certfile: '/etc/ssl-volume/certificate',
        port: 443
    },
    params: {
        fullReqLog: false, // true, if you want to output req.body into console (recommended false on production)
        fullErrors: false, // true, if you want to output error into response output (recommended false on production)
        sendAllEmailsTo: false, // email or null/false - sends all emails to specified email address
    },
    urls: {
        publicUrl: 'https://api.your-it-job-in-germany.com',
    },
    // see config.js - should be default
    //aejobs_microsite: {}
    //aexpro_microsite: {}
}
