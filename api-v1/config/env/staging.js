module.exports = {
    log: {
        pino: {
            init: {
                prettyPrint: false,
            },
        },
    },
    server: {
        ssl: true,
        keyfile: '/etc/ssl-volume/key',
        certfile: '/etc/ssl-volume/certificate',
        port: 443
    },
    params: {
        fullReqLog: false, // true, if you want to output req.body into console (recommended false on production)
        fullErrors: false, // true, if you want to output error into response output (recommended false on production)
        sendAllEmailsTo: false, // email or null/false - sends all emails to specified email address
    },
    urls: {
        publicUrl: 'https://api.staging.your-it-job-in-germany.com',
    },
    // see config.js - should be default
    aejobs_microsite: {
        aejobs_form: {
            step1: {
                response: {
                    mail_subject: {
                        de: "[stage] Willkommen bei Your IT Job in Germany",
                        en: "[stage] Welcome to Your IT Job in Germany"
                    }
                },
                datanotify: {
                    mail_subject: "[stage] New User Submission",
                }
            },
            step2: {
                response: {
                    mail_subject: {
                        de: "[stage] No Spam - Only Jobs",
                        en: "[stage] No Spam - Only Jobs"
                    }

                },
                datanotify: {
                    mail_subject: "[stage] New User Update",
                }
            }
        }
    },
    aexpro_microsite: {
        aexpro_form_berlin: {
            server_hostname: "https://stage-aexpro-berlin.itjobingermany.net",
            city_name: "Berlin",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "[stage] Willkommen bei Your IT Job in Berlin"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_to: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_subject: "[stage] New User",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "[stage][Berlin] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            }
        },
        aexpro_form_hamburg: {
            server_hostname: "https://stage-aexpro-hamburg.itjobingermany.net",
            city_name: "Hamburg",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "[stage] Willkommen bei Your IT Job in Hamburg"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_to: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_subject: "[stage] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "[stage][Hamburg] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            },
        },
        aexpro_form_leipzig: {
            server_hostname: "https://stage-aexpro-leipzig.itjobingermany.net",
            city_name: "Leipzig",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "[stage] Willkommen bei Your IT Job in Leipzig"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_to: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_subject: "[stage] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "[stage][Leipzig] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            }
        }
    }
}
