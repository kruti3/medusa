module.exports = {
    log: {
        pino: {
            init: {
                prettyPrint: true
            }
        }
    },
    server: {
        ssl: false,
        keyfile: '',
        certfile: '',
        port: 3100
    },
    urls: {
        publicUrl: 'http://localhost:3100'
    },
    params: {
        fullReqLog: true, // true, if you want to output req.body into console (recommended false on production)
        fullErrors: true, // true, if you want to output error into response output (recommended false on production)
        sendAllEmailsTo: false // email or null/false - sends all emails to specified email address
    },
    aejobs_microsite: {
        aejobs_form: {
            step1: {
                response: {
                    mail_from: "marco@qixs.me",
                    mail_subject: {
                        de: "[dev] Willkommen bei Your IT Job in Germany",
                        en: "[dev] Welcome to Your IT Job in Germany"
                    }
                },
                datanotify: {
                    mail_from: "marco@qixs.me",
                    mail_to: "marco@qixs.me",
                    mail_subject: "[dev] New User Submission",
                }
            },
            step2: {
                response: {
                    mail_from: "marco@qixs.me",
                    mail_subject: {
                        de: "[dev] No Spam - Only Jobs",
                        en: "[dev] No Spam - Only Jobs"
                    }

                },
                datanotify: {
                    mail_from: "marco@qixs.me",
                    mail_to: "marco@qixs.me",
                    mail_subject: "[dev] New User Update",
                }
            }
        }
    },
    aexpro_microsite: {
        aexpro_form_berlin: {
            server_hostname: "https://test-aexpro-berlin.itjobingermany.net",
            city_name: "Berlin",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "[dev] Willkommen bei Your IT Job in Berlin"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_to: "actinic@element115.de",
                    mail_subject: "[dev][Berlin] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "actinic@element115.de",
                    mail_subject: "[dev][Berlin] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            },
        },
        aexpro_form_hamburg: {
            server_hostname: "https://test-aexpro-hamburg.itjobingermany.net",
            city_name: "Hamburg",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "[dev] Willkommen bei Your IT Job in Hamburg"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_to: "actinic@element115.de",
                    mail_subject: "[dev][Hamburg] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "actinic@element115.de",
                    mail_subject: "[dev][Hamburg] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            },
        },
        aexpro_form_leipzig: {
            server_hostname: "https://test-aexpro-leipzig.itjobingermany.net",
            city_name: "Leipzig",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "[dev] Willkommen bei Your IT Job in Leipzig"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_to: "actinic@element115.de",
                    mail_subject: "[dev][Leipzig] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "actinic@element115.de",
                    mail_subject: "[dev][Leipzig] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            }
        }
    }
}
