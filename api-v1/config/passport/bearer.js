const _ = require('lodash')
const act = require('../../app/components/helperTools')
const enums = require('../../app/enums')
const errors = require('../../app/errors')
const BearerStrategy = require('passport-http-bearer').Strategy
const userService = require('../../app/services/userService')

module.exports = new BearerStrategy(
    (token, done) => {
        return userService.tokenDetail({ token })
            .then(_token => {
                if (!_.isObject(_token)) {
                    throw errors.user.tokenExpired()
                }

                if (_token.expiration < new Date().getTime()) {
                    throw errors.user.tokenExpired()
                }

                return userService.userDetail({ id: _token.userId }, { withRelated: enums.withRelated.user.authDetail })
            })
            .then(user => {
                if (!_.isObject(user)) {
                    throw errors.user.userDeleted()
                }

                if (!act.toBoolean(user.enabled)) {
                    throw errors.user.userDisabled()
                }

                return Promise.resolve(done(null, user))
            })
            .catch((err) => {
                return done(err, false)
            })
    }
)
