const path = require('path')
const fs = require('fs')
const _ = require('lodash')

const defaults = {
    root: path.normalize(`${__dirname}/..`),
}

/**
 * Default to all the environments, if you do not rewrite it, it became this
 */
const preDefaults = {
    auth: {
        accessToken: {
            secret: 'EyO7ab31eMBDWu7f3VLi',
            lifetimeInMinutes: 60,
        },
        refreshToken: {
            length: 30, // Number of characters of refresh token
        },
        resetToken: {
            length: 30,
            lifetimeInMinutes: 60 * 48,
        },
        confirmEmailToken: {
            length: 30,
            lifetimeInMinutes: 60 * 24 * 60,
        },
    },
    businessLogic: {
        password: {
            minLength: 8,
            maxLength: 1027,
        },
        registration: {
            confirmationEmail: {
                enabled: false, // send email with tokens which allows confirmation of email ownership
                // if set to false, then each new registered user is confirmed (confirmed = true)
            },
        },
        email: {
            defaultEmail: 'no-reply@example.com',
        },
    },
    i18n: {
        locales: ['en', 'de'],
        defaultLocale: 'en',
        cookieName: 'locale',
        extension: '.json',
    },
    log: {
        pino: {
            init: {
                prettyPrint: true, // it parses objects into more lines, human see can see it better, make it false on production environment
            },
        },
    },
    params: {
        fullReqLog: false, // true, if you want to output req.body into console (recommended false on production)
        fullErrors: true, // true, if you want to output error into response output (recommended false on production)
        sendAllEmailsTo: false, // email or null/false - sends all emails to specified email address
    },
    sendgrid: {
        enabled: true,
        apiKey: process.env.SENDGRID_API_KEY, // Sengrid Development Api Key (see bitbucket env vars)
    },
    server: {
        ssl: false,
        keyfile: '',
        certfile: '',
        port: 3100,
    },
    urls: {
        publicUrl: 'http://localhost:3100',
    },
    aejobs_microsite: {
        aejobs_form: {
            step1: {
                response: {
                    template: {
                        de: "20181009_aejobs_thankyou_step1_v1.html",
                        en: "20181009_aejobs_thankyou_step1_v1.html"
                    },
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "",
                    mail_subject: {
                        de: "Willkommen bei Your IT Job in Germany",
                        en: "Welcome to Your IT Job in Germany"
                    }
                },
                datanotify: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            },
            step2: {
                response: {
                    template: {
                        de: "20181009_aejobs_thankyou_step2_v1.html",
                        en: "20181009_aejobs_thankyou_step2_v1.html"
                    },
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "",
                    mail_subject: {
                        de: "No Spam - Only Jobs",
                        en: "No Spam - Only Jobs"
                    }

                },
                datanotify: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "New User Update",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            }
        }
    },
    aexpro_microsite: {
        aexpro_form_berlin: {
            server_hostname: "https://www.your-it-job-in-berlin.com",
            city_name: "Berlin",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "Willkommen bei Your IT Job in Berlin"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_to: "your-it-job-in-berlin@allgeier-experts.com",
                    mail_subject: "New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "[Berlin] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            },
        },
        aexpro_form_hamburg: {
            server_hostname: "https://www.your-it-job-in-hamburg.com",
            city_name: "Hamburg",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "Willkommen bei Your IT Job in Hamburg"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_to: "your-it-job-in-hamburg@allgeier-experts.com",
                    mail_subject: "New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "[Hamburg] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            },
        },
        aexpro_form_leipzig: {
            server_hostname: "https://www.your-it-job-in-leipzig.com",
            city_name: "Leipzig",
            step1: {
                response: {
                    template: {
                        de: "20190301_aexpro_thankyou_step1_v1.html"
                    },
                    mail_from: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_to: "",
                    mail_subject: {
                        de: "Willkommen bei Your IT Job in Leipzig"
                    }
                },
                datanotify: {
                    mail_from: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_to: "your-it-job-in-leipzig@allgeier-experts.com",
                    mail_subject: "New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                },
                datanotify_neo: {
                    mail_from: "contact@allgeier-neo.com",
                    mail_to: "contact@allgeier-neo.com",
                    mail_subject: "[Leipzig] New User Submission",
                    text: "JSON Data of Submission",
                    html: "<html>JSON Data of Submission</html>"
                }
            }
        }
    },
    allgeier: {
        cockpit: {
            enabled: true,
            appServerUrl: process.env.ALLGEIER_APPSERVER_URL, // see bitbucket env vars: ALLGEIER_APPSERVER_URL_<environment>
            appServerAccessToken: process.env.ALLGEIER_APPSERVER_TOKEN // see bitbucket env vars: ALLGEIER_APPSERVER_TOKEN_<environment>
        },
        // no API available
        hr4you: {
            enabled: false,
            appServerUrl: false,
            appServerAccessToken: false
        }
    }
}

module.exports = preDefaults // This will be rewrited in next lines, but if we want IDE to suggest us all the keys, we must use this ugly hack

const envs = {}
fs.readdirSync(`${__dirname}/env`).forEach((file) => {
    if (file.indexOf('.js')) {
        envs[file.substring(0, file.length - 3)] = _.merge(
            {},
            preDefaults,
            require(`${__dirname}/env/${file}`), // eslint-disable-line global-require, import/no-dynamic-require
            defaults)
    }
})

module.exports = envs[process.env.NODE_ENV || 'development']
