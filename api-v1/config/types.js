/**
 * Express request object
 * @typedef {Object} Req
 * @property {Object} [body]
 * @property {Object} [params]
 * @property {Object} [query]
 * @property {Function} [get]
 * @property {string} [protocol]
 * @property {string} [originalUrl]
 * @property {number} [userId]
 * @property {Object} [i18n]
 *
 */

/**
 * Express response object
 * @typedef {Object} Res
 * @property {Object} out
 * @property {Function} json
 * @property {Function} status
 */

/**
 * Express next function
 * @typedef {Function} Next
 */

/**
 *
 * @typedef {Object} DefaultModel
 */


/**
 *
 * @typedef {Object} BsModel
 */
