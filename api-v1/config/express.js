const express = require('express')
const compression = require('compression')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const config = require('config')
const i18n = require('i18n-2')

module.exports = function (app, passport) {
    // Compression middleware (should be placed before express.static)
    app.use(compression({
        threshold: 512,
    }))

    // Static files middleware
    app.use(express.static(`${config.root}/public`))

    app.use(morgan('combined'))

    // bodyParser should be above methodOverride
    app.use(bodyParser.urlencoded({
        extended: true,
    }))

    app.use(bodyParser.json())

    // use passport session
    app.use(passport.initialize())
    app.use(passport.session())

    // i18n
    i18n.expressBind(app, config.i18n)

    app.use((req, res, next) => {
        req.i18n.setLocaleFromQuery()
        req.i18n.setLocaleFromCookie()
        next()
    })
}
