module.exports = {
    userCockpit: {
        userNotFound: 1001,
        userAlreadyExists: 1002,
        userUpdateFailed: 1003,
    },
    userHR4You: {
        userNotFound: 1101,
        userAlreadyExists: 1102,
        userUpdateFailed: 1103,
    },
    language: {
        languageNotFound: 7040,
    },
}
