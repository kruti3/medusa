const errorCodesEnum = require('./data/errorCodesEnum')

module.exports = {
    countryCode: {
        de: 'de_DE',
        en: 'en_US',
    },
    email: {
        type: {
            aejobsStep1: 'aejobs_step1',
            aejobsStep2: 'aejobs_step2',
            aejobsAdminInfo1: 'aejobs_admin_info1',
            aejobsAdminInfo2: 'aejobs_admin_info2',
            aexproStep1: 'aexpro_step1',
            aexproAdminInfo1: 'aexpro_admin_info1'
        },
    },
    errorCodes: errorCodesEnum,
    languages: {
        de: 'de',
        en: 'en',
    },
    order: {
        asc: 'asc',
        desc: 'desc',
    },
    props: {},
    roles: {
        names: {
            admin: 'ADMIN',
            userCockpit: 'USER_COCKPIT',
            userHR4You: 'USER_HR4YOU',
            superadmin: 'SUPERADMIN',
        },
    },
    tokens: {
        type: {
            accessToken: 'accessToken',
            refreshToken: 'refreshToken',
            resetToken: 'resetToken',
            confirmEmailToken: 'confirmEmailToken',
        }
    }
}
