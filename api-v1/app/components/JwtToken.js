const jwt = require('jwt-simple')
const _ = require('lodash')

class JwtToken {
    /**
     *
     * @param data
     * @param secret
     * @param {Number=} options.lifetimeInMinutes
     */
    constructor(data, secret, _options = {}) {
        const options = _.cloneDeep(_options)
        if (!_.isNumber(options.lifetimeInMinutes)) {
            options.lifetimeInMinutes = 60
        }

        this.data = data

        const now = new Date()
        this.iat = data.iat ? data.iat : now.getTime()
        this.exp = data.exp ? data.exp :
            new Date(this.iat + (options.lifetimeInMinutes * 1000 * 60)).getTime()

        this.secret = secret
    }

    isExpired() {
        return this.exp < new Date().getTime()
    }

    pack() {
        return jwt.encode(_.merge({}, this.data, { iat: this.iat }, { exp: this.exp }), this.secret)
    }

    static unpack(packed, secret) {
        let data
        try {
            data = jwt.decode(packed, secret)
        } catch (ex) {
            return false
        }
        const unpacked = new this(data)
        delete unpacked.data.iat
        delete unpacked.data.exp
        return unpacked
    }
}

module.exports = JwtToken
