const config = require('../../../../config/config')
const _ = require('lodash')
const serializers = require('./serializers')
const pino = require('pino')(_.merge({ serializers }, config.log.pino.init))

module.exports = pino
