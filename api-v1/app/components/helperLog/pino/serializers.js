const _ = require('lodash')
const act = require('../../helperTools')

module.exports = {
    reqId(val) {
        return val
    },
    err(obj) {
        return {
            err: obj,
            errMessage: _.get(obj, 'message'),
            errStack: _.get(obj, 'stack'),
            errStringified: JSON.stringify(obj),
        }
    },
    processEnv(obj) {
        return {
            nodePath: _.get(obj, 'NODE_PATH'),
            nodeEnv: _.get(obj, 'NODE_ENV'),
        }
    },
    req(obj) {
        const body = _.cloneDeep(_.get(obj, 'body'))
        const query = _.cloneDeep(_.get(obj, 'query'))

        if (_.get(body, 'password')) {
            delete body.password
        }

        if (_.get(query, 'password')) {
            delete query.password
        }

        if (_.get(body, 'passwordCheck')) {
            delete body.passwordCheck
        }

        if (_.get(query, 'passwordCheck')) {
            delete query.passwordCheck
        }

        return {
            body,
            query,
            url: act.fullUrlFromReq(obj),
            method: _.get(obj, 'method'),
        }
    },
    res(obj) {
        return {
            out: _.get(obj, 'out'),
        }
    },
}
