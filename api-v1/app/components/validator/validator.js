const _ = require('lodash')
const validator = require('./js-object-validator')
const validations = require('../../../config/validation/validations_formData')
const errors = require('../../errors')

/**
 *
 * @param {!Object} obj Object to be validated
 * @param {!Object} _validations The object contains rules for validation
 */
exports.validate = (obj, _validations) => {
    return validator.validate(obj, _validations).then(result => {
        if (!_.isEmpty(result)) {
            throw errors.new.badRequest(result)
        }

        return Promise.resolve()
    })
}

exports.validations = validations
