/**
 * Created by Lukas Koci <lukas.koci@ackee.cz> on 19.09.2017.
 */

const _ = require('lodash')
const moment = require('moment')

/**
 * Validate whether the default values are in array.
 * @param {Array} arr
 * @param {Array} defaultValues
 * @returns {boolean}
 */
exports.areIn = (arr, defaultValues) => {
    return _.every(arr, val => {
        return _.includes(defaultValues, val)
    })
}

/**
 * Validates whether the value is between min and max (including mix and max).
 * @param {number} value
 * @param {number} min
 * @param {number} max
 * @returns {boolean}
 */
exports.isBetween = (value, min, max) => {
    return value >= min && value <= max
}

/**
 * Validates whether the value is in date time format.
 * @param {string} value
 * @returns {boolean}
 */
exports.isDate = (value) => {
    return moment(value).isValid()
}

/**
 * Validates whether the value + format is in date time format.
 * @param {string} value
 * @param {string} args
 * @returns {boolean}
 */
exports.isDateWithFormat = (value, args) => {
    return moment(value, args.format).isValid()
}

/**
 * Validates whether the values is in array.
 * @param {string} value
 * @param {Array} defaultValues
 * @returns {boolean}
 */
exports.isIn = (value, defaultValues) => {
    return _.includes(defaultValues, value)
}

/**
 * Validates whether the value is not empty.
 * @param {*} value
 * @returns {boolean}
 */
exports.isNotEmpty = (value) => {
    return !_.isEmpty(value)
}

/**
 * Validates whether the value is not empty and is string.
 * @param {string} value
 * @returns {boolean}
 */
exports.isNotEmptyString = (value) => {
    return exports.isNotEmpty(value) && _.isString(value)
}

/**
 * Validates whether the value is string or null.
 * @param {*} value
 * @returns {boolean}
 */
exports.isStringOrNull = (value) => {
    return _.isString(value) || _.isNull(value)
}

exports.isZeroInt = (value) => {
    return value === 0 && _.isInteger(value) && exports.isNotEmpty(value)
}

exports.isNotZeroInt = (value) => {
    return value !== 0 && _.isInteger(value) && exports.isNotEmpty(value)
}