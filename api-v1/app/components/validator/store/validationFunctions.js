/**
 * Created by Lukas Koci <lukas.koci@ackee.cz> on 09.11.2017.
 */

const _ = require('lodash')
const validator = require('./validators')

const baseValidations = {
    array() {
        return {
            assert: _.isArray,
            message: 'alert.isArray',
        }
    },
    int() {
        return {
            assert: _.isInteger,
            message: 'alert.isInt',
        }
    },
    zeroInt() {
        return {
            assert: validator.isZeroInt,
            message: 'alert.isInt',
        }
    },
    notZeroInt() {
        return {
            assert: validator.isNotZeroInt,
            message: 'alert.isInt',
        }
    },
    optional() {
        return {
            optional: true,
        }
    },
    required() {
        return {
            required: true,
        }
    },
    string() {
        return {
            assert: validator.isStringOrNull,
            message: 'alert.isString',
        }
    },
}

exports.optionalInt = () => {
    return _.merge(baseValidations.int(), baseValidations.optional())
}

exports.optionalArray = () => {
    return _.merge(baseValidations.array(), baseValidations.optional())
}

exports.optionalString = () => {
    return _.merge(baseValidations.string(), baseValidations.optional())
}

exports.requiredArray = () => {
    return _.merge(baseValidations.array(), baseValidations.required())
}

exports.requiredIntZero = () => {
    return _.merge(baseValidations.zeroInt(), baseValidations.required())
}

exports.requiredNotIntZero = () => {
    return _.merge(baseValidations.notZeroInt(), baseValidations.required())
}

exports.requiredInt = () => {
    return _.merge(baseValidations.int(), baseValidations.required())
}

exports.requiredString = () => {
    return _.merge(baseValidations.string(), baseValidations.required())
}
