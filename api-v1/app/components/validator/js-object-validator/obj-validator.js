// custom version instead of npm module (outdated)
'use strict';

const _ = require('lodash');
const validatorjs = require('validator');

const OBJECT_IT_SELF = '_';

/**
 * Descriptors: Hash object, key:Object property to validate, value: array of objects {assert: ASSERT, message: MESSAGE}
 *  ASSERT is any function accepting a value, returning a promise resolving TRUE/FALSE, or rejects with error on error.
 *  MESSAGE is a message passed along to response if ASSERT results in false.
 */

exports.validate = (object, objectValidationDescriptor) => {

    const result = {};
    const promises = [];

    _.map(objectValidationDescriptor, (fieldDescriptors, key) => {
        promises.push(exports.validateObjectWithFieldDescriptors(object, fieldDescriptors, key)
        .then(res => {
                //Only store result for that key iff there are any errors
                if (!_.isEmpty(res)) {
                    // Multiple descriptors still describe the same object -> merge
                    if (_.isArray(objectValidationDescriptor)) {
                        // if all of results are a String, means all are errors for that very object,
                        // store them all in special case variable merged (Union errors)
                        if (_.every(res, _.isString)) {
                            result[OBJECT_IT_SELF] = _.chain(result).get(OBJECT_IT_SELF, []).union(res).value();
                        // Otherwise merge as normal array
                        } else {
                            _.merge(result, res);
                        }
                    } else {
                        result[key] = res;
                    }
                }
            })
        );
    });

    return Promise.all(promises)
        .then(() => result);
};

exports.validateArray = (array, descriptors) => {

    return Promise.all(
        _.map(array, child =>
            exports.validate(child, descriptors)
        )
    )
        .then(result => _.every(result, _.isEmpty) ? [] : result);
};

/**
 * Validates object with given set of Descriptors.
 * @param object to validate
 * @param descriptors [] or {}
 * @param key Name of the property being validated
 * @returns {Promise.<TResult>} [] of error messages
 */
exports.validateObjectWithFieldDescriptors = (object, descriptors, key) => {

    let collectionKey = exports.resolveCollection(key);
    const isCollection = !!collectionKey;
    if (isCollection) {
        return exports.validateArray(_.get(object, collectionKey, []), descriptors);
    }
    const isObject = !isCollection && !exports.isDescriptor(descriptors);
    if (isObject) {
        return exports.validate(_.get(object, key, object), descriptors);
    }

    const results = _.map(_.castArray(descriptors), descriptor =>
        exports.keyDescriptorResult(object, descriptor, key)
    );

    //Return errors in given order, skip passed asserts
    return Promise.all(results)
        .then(results => {
            return _(results)
                    .flatten()  // may be array of arrays, in which case, it's been an object collection validation
                    .filter(x => x !== true) // skip okeys
                    .value()
            }
        )
};

/**
 * Validates actual property key for a given object.
 * @param object To validate
 * @param descriptor
 * @param key of object being validated
 * @returns true|"error_message"
 */
exports.keyDescriptorResult = (object, descriptor, key) => {

    return exports
        .invokeDescriptorValidation(descriptor, object, key)
        .then(result => {
            return result.ok === true
                ? true
                : result.message
        });
};

exports.invokeDescriptorValidation = (descriptor, object, key) => {

    const value = object[key];
    const resolved  = exports.resolveDescriptor(descriptor);

    const validator = resolved.validator;
    let required = resolved.required;

    const optionalResult = resolved.optional(value);

    if (typeof optionalResult === 'boolean') {
        if (optionalResult) {
            return Promise.resolve(
                _.assign({}, resolved, {
                    ok: true,
                })
            )
        } else {
            required = required || true;
        }
    }
    // NOTE: cast value to .toString() because validator ONLY parses strings for types
    if (!_.isUndefined(required) && _.isUndefined(value)) {
        const message = _.isString(required) ? required : 'required';
        return Promise.resolve(_.assign({}, resolved, {
            ok: false,
            message,
        }));
    }

    const args = _(resolved.args)
        .concat([{
            data: object,
            key,
        }])
        .value();
    const callArgs = _.concat([value.toString()], args);
    return Promise.resolve(
        validator ? validator.apply(this, callArgs) : true
    )
        .then(ok => _.assign({}, resolved, {ok}));
};


/**
 * Recursively replaces all strings with given replace string.
 * @param result validation result
 * @param messages Hash object key:replace_value
 * @returns {} modified result
 */
exports.replaceMessages = (result, messages) => {

    return exports.replaceMessagesFunc(result,
        function replaceKey(key) {
            var replace = _.get(messages, key);
            if (replace) {
                return replace;
            }
            return key;
        });
};

/**
 * Recursively replaces all strings with given replace function.
 * @param result validation result
 * @param messages function translating string->string
 * @returns {} modified result
 */
exports.replaceMessagesFunc = (result, replaceFunc) => {

    return replaceRec(Object.assign({}, result));

    function replaceRec(result) {

        if ('string' === typeof result) {
            return replaceFunc(result);
        }

        _.map(result, (value, prop) => {
            result[prop] = replaceRec(value);
        });

        return result;
    }
};

/**
 * Resolve key name, whether it is an collection of subobjects, or not.
 * @param {String} key
 * @returns {Boolean|String} False is key is not collection. Collection name as string, if yes.
 */
exports.resolveCollection = key => {
    let match = (String(key || '')).match(/^(\w+)\.children$/);
    if (!match) {
        return match;
    }
    return match[1];
};

exports.isDescriptor = descriptor => {
    return _.isArray(descriptor)
        ? Boolean(exports.resolveDescriptor(_.head(descriptor)))
        : Boolean(exports.resolveDescriptor(descriptor));
};

/**
 * Resolve descriptor properties. Which validator function to call, arguments to call this validator with, message
 * @param _descriptor
 * @return {*} Resolved descriptor
 */
exports.resolveDescriptor = _descriptor => {
    const descriptor = _descriptor || {};
    const name = descriptor.assert
              || descriptor.validator
              || descriptor;

    const validatorJsFunc = validatorjs[name];
    const isCustom = _.isFunction(name);
    const validator =  exports.resolveDescriptor.resolveValidator(descriptor);
    const required = descriptor.required;
    const optional = exports.resolveDescriptor.resolveOptional(descriptor);
    const typeHint = descriptor.typeHint || 'object';

    if (!validator && !required) {
        return false;
    }

    const args = _.get(descriptor, 'args') || [];
    const message = _.get(descriptor, 'message') || (validator || '').name || (_.isString(name) ? `${name}Rejected` : null) || 'invalid';
    return {
        validator,
        isCustom,
        args,
        message,
        required,
        optional,
        typeHint,
    };
};

/**
 * @param descriptor
 * @returns {Function}
 */
exports.resolveDescriptor.resolveValidator = (descriptor) => {

    const resolvers = [
        (f) => _.isFunction(f)
            ? f
            : null,
        (f) => _.isFunction(_.get(f, 'assert'))
            ? f.assert
            : null,
        (f) => _.isFunction(_.get(f, 'validator'))
            ? f.validator
            : null,
        (f) => _.isString(f) && _.isFunction(validatorjs[f])
            ? validatorjs[f]
            : null,
        (f) => _.has(f, 'assert') && _.isString(f.assert) && _.isFunction(validatorjs[f.assert])
            ? validatorjs[f.assert]
            : null,
        (f) => _.has(f, 'validator') && _.isString(f.validator) && _.isFunction(validatorjs[f.validator])
            ? validatorjs[f.validator]
            : null,
    ];
    let validator = null;
    _.each(resolvers, resolver => {
        validator = resolver(descriptor);
        return !validator;
    });
    return _.isFunction(validator) ? validator : null;
};

/**
 * @param {Object} descriptor
 * @returns {boolean}
 */
exports.resolveDescriptor.resolveOptional = (descriptor) => {

    const defaultOptional = _.isUndefined;
    return _.flow(
        (descr => (descr || {}).optional),
        (opt) => {
            switch (typeof opt) {
                case 'function':
                    return opt;
                case 'boolean':
                    return opt ? defaultOptional : () => false;
                default:
                    return () => undefined;
            }
        }
    )(descriptor);
};
