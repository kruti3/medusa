const sgMail = require('@sendgrid/mail');
const path = require('path');
const config = require('../../../config/config')
const templatepath = path.join(__dirname, '../../../templates/sendgrid/');
const fs = require('fs');
sgMail.setApiKey(config.sendgrid.apiKey);

function getDateTimeAsString() {
    return (new Date()).toISOString().substring(0, 10) + "_" + (new Date().getHours()) + (new Date().getUTCMinutes()) + (new Date().getUTCSeconds())
}

function loadTemplateToString(fname,locale) {
    const filename  = path.join(templatepath + locale + "/" + fname);
    //console.log("loadTemplateToString: filename -> " + filename)
    var mycontent = ''
    if (fs.existsSync(filename)) {
        mycontent = fs.readFileSync(filename, "utf8");
    } else {
        console.log("FATAL: file not found -> " + filename)
    }
    return mycontent
}

// sendEmailToUser Cockpit
function cockpitSendEmailToUser(payload,mstep,mtype,apiError=false) {
    if (mstep === "none") return
    // load template
    const locale = payload.metadata.locale
    const template = loadTemplateToString(config.aejobs_microsite.aejobs_form[mstep][mtype]['template'][locale],locale)
    const msg = {
        to: payload.applicant.email,
        from: config.aejobs_microsite.aejobs_form[mstep][mtype].mail_from,
        subject: config.aejobs_microsite.aejobs_form[mstep][mtype].mail_subject[locale],
        html: template,
        substitutionWrappers: ['<%', '%>'],
        substitutions: {
            first_name: payload.applicant.first_name,
            last_name: payload.applicant.last_name,
        },
    };
    sgMail
    .send(msg)
    //.then(() => console.log('Mail sent successfully: '+mstep+' - '+mtype))
    .catch(error => console.error(error.toString()));
}

// sendEmailToAdminWithData
function cockpitSendEmailToAdminWithData(payload,mstep,mtype,apiError=false,apiErrorPayload={}) {
    if (mstep === "none") return
    let extraSubject = ' [' + mstep + ']'
    extraSubject += '[' + payload.applicant.email + ']'
    let extraAttachments = []

    // assume payload is JSON
    const s1   = JSON.stringify(payload)
    const buf1 = Buffer.from(s1.toString(), 'ascii')
    const attachment1 = buf1.toString('base64')
    const attach1 = { content: attachment1, filename: getDateTimeAsString() + '.json.txt' }

    // apiError? create 2 payloads
    if (apiError === true) {
        // append some info to subject
        extraSubject += '[API error detected]'

        // assume apiError is not object
        const s2  = JSON.stringify(apiErrorPayload)
        const buf2 = Buffer.from(s2.toString(), 'ascii')
        const attachment2 = buf2.toString('base64')
        const attach2 = { content: attachment2, filename: 'API_ERROR_payload_' + getDateTimeAsString() + '.json.txt' }
        extraAttachments.push(attach1)
        extraAttachments.push(attach2)
    } else {
        extraAttachments.push(attach1)
    }

    const msg = {
        to: config.aejobs_microsite.aejobs_form[mstep][mtype].mail_to,
        from: config.aejobs_microsite.aejobs_form[mstep][mtype].mail_from,
        subject: config.aejobs_microsite.aejobs_form[mstep][mtype].mail_subject + extraSubject,
        text: config.aejobs_microsite.aejobs_form[mstep][mtype].text,
        html: config.aejobs_microsite.aejobs_form[mstep][mtype].html,
        attachments: extraAttachments
    };
    sgMail
    .send(msg)
    //.then(() => console.log('Mail sent successfully: '+mstep+' - '+mtype))
    .catch(error => console.error(error.toString()));
}


// region: HR4You
function hr4youPromiseSG(sgdata,resdata) {

    // API call to SendGrid
    let sgResponse = {
        response: {
            endpoint: "HR4You_SG",
            external_key: 0,
            status: 200,
            payload: {
                message: '',
                mtarget: resdata.mtarget,
                mform: resdata.mform,
                mstep: resdata.mstep,
                mtype: resdata.mtype
            }
        }
    }
    return sgMail.send(sgdata)
        .then(([response, body]) => {
            sgResponse.response.status = response.statusCode
            sgResponse.response.payload.message = response.statusMessage
            return sgResponse
        })
        .catch(function (error) {
            // error is an instance of SendGridError
            // The full response is attached to error.response
            sgResponse.response.status = error.statusCode
            sgResponse.response.payload.message = error.statusMessage
            return sgResponse
        })
    return sgResponse
}

// sendEmailToUser HR4You
function hr4youSendEmailToUser(payload,mtarget,mform,mstep,mtype) {
    if (mstep === "none") return
    // load template
    const locale = payload.metadata.locale
    const template = loadTemplateToString(config[mtarget][mform][mstep][mtype]['template'][locale],locale)
    const msg = {
        to: payload.applicant.email,
        from: config[mtarget][mform][mstep][mtype].mail_from,
        subject: config[mtarget][mform][mstep][mtype].mail_subject[locale],
        html: template,
        substitutionWrappers: ['<%', '%>'],
        substitutions: {
            first_name: payload.applicant.first_name,
            last_name: payload.applicant.last_name,
            server_hostname: config[mtarget][mform].server_hostname,
            city_name: config[mtarget][mform].city_name
        },
    }
    const resdata = {
        mtarget: mtarget,
        mform: mform,
        mstep: mstep,
        mtype: mtype
    }

    return hr4youPromiseSG(msg,resdata)

}

// sendEmailToAdminWithData
function hr4youSendEmailToAdminWithData(payload,mtarget,mform,mstep,mtype) {
    if (mstep === "none") return
    let extraSubject = ' [' + mstep + ']'
    extraSubject += '[' + payload.applicant.email + ']'
    let extraAttachments = []

    // assume payload is JSON
    // JSON.stringify({ a:1, b:2, c:3 }, null, 4)
    const s1   = JSON.stringify(payload, null, 2)
    const buf1 = Buffer.from(s1.toString(), 'ascii')
    const attachment1 = buf1.toString('base64')
    const attach1 = { content: attachment1, filename: getDateTimeAsString() + '.json.txt' }

    extraAttachments.push(attach1)

    const msg = {
        // example config.aexpro_microsite.aexpro_form_berlin.step1.response
        to: config[mtarget][mform][mstep][mtype].mail_to,
        from: config[mtarget][mform][mstep][mtype].mail_from,
        subject: config[mtarget][mform][mstep][mtype].mail_subject + extraSubject,
        text: config[mtarget][mform][mstep][mtype].text,
        html: config[mtarget][mform][mstep][mtype].html,
        attachments: extraAttachments
    }
    const resdata = {
        mtarget: mtarget,
        mform: mform,
        mstep: mstep,
        mtype: mtype
    }
    return hr4youPromiseSG(msg,resdata)
}
// endregion

// exports
module.exports.mailConfig = config;
module.exports.hr4youPromiseSG = hr4youPromiseSG;
module.exports.loadTemplateToString = loadTemplateToString;
module.exports.cockpitSendEmailToUser = cockpitSendEmailToUser;
module.exports.hr4youSendEmailToUser = hr4youSendEmailToUser;
module.exports.cockpitSendEmailToAdminWithData = cockpitSendEmailToAdminWithData;
module.exports.hr4youSendEmailToAdminWithData = hr4youSendEmailToAdminWithData;
