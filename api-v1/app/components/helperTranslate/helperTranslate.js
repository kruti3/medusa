const config = require('../../../config/config')
const I18n = require('i18n-2')

/**
 * Translate key by locale
 * @param {!string} key Text find by key that would be translated
 * @param {string=} locale The language of translation. If there is none found, it uses the default
 * @param {Array.<string>} replacements If you have dynamic replacements in your translations, you can use this
 * @return {!string}
 */
exports.translate = (key, locale = '', replacements = []) => {
    const i18n = new I18n(config.i18n)
    i18n.setLocale(locale)
    return i18n.__(key, ...replacements)
}
