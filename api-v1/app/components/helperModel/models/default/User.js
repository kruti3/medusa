/**
 * Basic user object
 * @typedef {Object} User
 * @property {!number} [id]- Unique identitifaction
 * @property {!string} [email] - Email and username in once
 * @property {!boolean} [enabled] - True, if user can access the system
 * @property {!boolean} [confirmed] - True, if validation (i.e. through email) was successfull
 * @property {Date} [createdAt]
 * @property {Date} [updatedAt]
 *
 * @property {?string} [name] - Name of user
 */

/**
 * Object for creation user
 * @typedef {Object} CreatableCockpitUser
 * @property {!string} email
 * @property {!string} first_name
 * @property {!string} last_name
 */

/**
 * Object for updating user
 * @typedef {Object} UpdatableCockpitUser
 *
 * @property {?string} [external_id] - Id in Cockpit
 * @property {?string} [first_name] - First Name of user
 * @property {?string} [last_name] - Last Name of user
 */
