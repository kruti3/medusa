/**
 * Basic token object
 * @typedef {Object} Token
 * @property {!number} [id] - Unique identitifaction
 * @property {!string} [token] - the token itself as string representation
 * @property {!string} [tokenHash] - database hash version, not important for end user, only for indexing database
 * @property {!string} [type] - type of token (i.e. accessToken)
 * @property {!number} [userId] - ID of user it is linked to
 * @property {Date} [lastUse]
 * @property {Date} [createdAt]
 * @property {Date} [updatedAt]

 * @property {!Date} [expiration] - tokens with expiration lower than actual date are no longer usable. Null, if it never expires.
 */

/**
 * Token object used for creation
 * @typedef {Object} CreatableToken
 * @property {!string} token - the token itself as string representation
 * @property {!string} type - type of token (i.e. accessToken)
 * @property {!number} userId - ID of user it is linked to

 * @property {!Date} [expiration] - tokens with expiration lower than actual date are no longer usable. Null, if it never expires.
 */

/*
module.exports = bookshelf => {
    const Token = bookshelf.Model.extend({
        tableName: 'tokens',
        hasTimestamps: true,
        hidden: ['tokenHash'],
    })

    return bookshelf.model('Token', Token)
}
*/