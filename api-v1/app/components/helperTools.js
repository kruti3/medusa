const _ = require('lodash')
const bcryptjs = require('bcryptjs')
const crypto = require('crypto')

/**
 * Encode plain password
 * @param {!string} plain Plain password for bcrypt
 */
exports.encodeBcrypt = plain => {
    return bcryptjs.genSalt(10).then(salt => {
        return bcryptjs.hash(plain, salt)
    })
}

exports.compareBcrypt = (plain, hashed) => {
    return bcryptjs.compare(plain, hashed)
}

/**
 * Gets full URL from express Req object
 * @param {Req} req
 * @return {!string}
 */
exports.fullUrlFromReq = req => {
    return `${req.protocol}://${req.get('host')}${req.originalUrl}`
}

/**
 * Transforms (usually database) object to pure JSON object. If it does not have toJSON or object does not exist, it returns object itself
 * @param {?Object=} obj Object to transform to JSON
 * @param {!Object=} options Object that is passed to toJSON method
 * @return {*}
 */
exports.nullOrToJSON = (obj, options = {}) => {
    if (!obj || !obj.toJSON) {
        return obj
    }

    return obj.toJSON(options)
}

/**
 * Snakelize object keys or objects in array from camelCase to snakeCase. DOES NOT have nested snakelization.
 * @param {Object|Array.<Object>} obj Object to be snakelized
 * @return {Object|Array.<Object>} Snakelized object
 */
exports.snakelize = obj => {
    if (_.isArray(obj) === true) {
        const out = []
        obj.forEach(item => {
            out.push(exports.snakelize(item))
        })

        return out
    }

    if (!obj || !_.isObject(obj)) {
        return obj
    }

    const out = {}
    Object.keys(obj).forEach(key => {
        out[_.snakeCase(key)] = obj[key]
    })
    return out
}

/**
 * If object contains transacting, it pass it, if it does not contains transacting, it creates one
 * @param {Knex} knex
 * @param {Object=} params Object that can contain transacting
 * @return {function(*)}
 */
 // no knex used
/*exports.transacted = (knex, params) => {
    return fn => {
        if (params && params.transacting) {
            return fn(params.transacting)
        }
        return knex.transaction(t => fn(t))
    }
}*/

/**
 * Generates random alphanumeric code*
 * @param {!number} num Number of characters
 * @returns {string} Random alphanumeric
 */
exports.generateRandomAlphanumeric = (num) => {
    return crypto.randomBytes(Math.ceil(num * (3 / 4)))
    // convert to base64 format
        .toString('base64')
        // return required number of characters
        .slice(0, num)
        // replace '+' with '0'
        .replace(/\+/g, '0')
        .replace(/\//g, '0')
}

/**
 * Create sha512 hash from string
 * @param {!string} input
 * @return {!string}
 */
exports.createHash = input => {
    const hash = crypto.createHash('sha512')
    hash.update(input)
    return hash.digest('hex')
}

/**
 * Transforms any "kind of boolean" into native javascript boolean
 * @param {*} obj
 * @return {!boolean}
 */
exports.toBoolean = obj => {
    return (obj === true || obj === 'true' || obj === '1' || obj === 1)
}

/**
 * Create new date that has added time to current date
 * @param minutes Minutes that will be added to current time
 * @return {Date} Date that is required minutes in future
 */
exports.createDateWithMinutesFromNow = minutes => {
    return (new Date(Date.now() + (minutes * 60 * 1000)))
}

/**
 * Insert or Update, based on if row is selected in select query
 * @param {Function<Promise.<Object>>} methods.select
 * @param {Function} methods.insert
 * @param {Function} methods.update
 * @param {Object=} options
 * @returns {Promise.<Object>}
 */
exports.updateOrInsert = (methods, options = {}) => { // eslint-disable-line no-unused-vars
    return methods.select().then(obj => {
        if (!obj) {
            return methods.insert()
        }

        if (!methods.update) {
            return obj
        }
        return methods.update(obj)
    })
}


//exports.sqlColumnsListPromise = (knex, table) => knex(table).columnInfo().then(x => _.keys(x).map(_.camelCase))

exports.attributes = (bookshelf, options, self, params) => {
    const attributes = bookshelf.Model.prototype.toJSON.call(
        self, _.merge(
            options || {}, {
                omitPivot: true,
            }
        )
    )

    _.map(_.get(params, 'jsonColumns', []), (column) => {
        if (attributes[column]) {
            attributes[column] = _.isArray(attributes[column]) ? attributes[column] : JSON.parse(attributes[column])
        }
        if (_.isArray(attributes[column])) {
            attributes[column] = _.filter(attributes[column], attr => (attr !== ''))
        }
    })

    _.map(_.get(params, 'toBoolean', []), (column) => {
        if (_.has(attributes, column)) {
            attributes[column] = exports.toBoolean(attributes[column])
        }
    })

    return attributes
}
