const _ = require('lodash')

function findRole(roles, roleName) {
    return _.some(roles, { name: roleName })
}

exports.hasRole = (_roleNames, path = 'user.roles') => req => {
    const roleNames = _.castArray(_roleNames)
    let foundRole = false

    roleNames.forEach(roleName => {
        if (findRole(_.get(req, path), roleName)) {
            foundRole = true
        }
    })

    if (foundRole) {
        return Promise.resolve()
    }

    return Promise.reject(new Error(`User does not have ${roleNames} role`))
}

exports.isOwner = () => req => {
    return exports.isSameInReq(req, 'user.id', 'userId')
}

exports.isSameInReq = (req, first, second) => {
    if (_.toString(_.get(req, first)) === _.toString(_.get(req, second))) {
        return Promise.resolve()
    }

    return Promise.reject(new Error(`The option in ${first} is not same as at ${second}`))
}
