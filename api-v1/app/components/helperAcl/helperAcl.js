const _ = require('lodash')
const errors = require('../../errors')
const rules = require('./data/rules')

exports.rules = rules

const strategies = {
    and: {
        successFast: false,
        failFast: true,
    },
    or: {
        successFast: true,
        failFast: false,
    },
}

// This is the evaluation supporting failFast (first failure fails the chain) and successFast (first success end the chain with that success)
const next = (i, strategy, tasks, args) => {
    const task = tasks[i]
    const hasNext = !!tasks[i + 1]
    return task(...args)
        .then(result => (!hasNext || strategy.successFast ? result : next(i + 1, strategy, tasks, args)))
        .catch(error => (!hasNext || strategy.failFast ? Promise.reject(error) : next(i + 1, strategy, tasks, args)))
}

const mainNext = (i, strategy, tasks, args) => {
    tasks[i] = _.castArray(tasks[i])
    const task = () => next(0, strategies.and, tasks[i], args)
    const hasNext = !!tasks[i + 1]
    return task()
        .then(result => (!hasNext || strategy.successFast ? result : mainNext(i + 1, strategy, tasks, args)))
        .catch(error => (!hasNext || strategy.failFast ?
            Promise.reject(error) :
            mainNext(i + 1, strategy, tasks, args))
        )
}

exports.mw = function mw() {
    return (req, res, nxt) => {
        mainNext(0, strategies.or, arguments, [req, res]).then(() => { // eslint-disable-line prefer-rest-params
            return nxt()
        }).catch(err => {
            return nxt(errors.new.forbidden(err.message))
        })
    }
}
