const _ = require('lodash')
const config = require('../../../config/config')
const rp = require('request-promise');
const helperLog = require('../helperLog')

// Cockpit AddProfile
exports.externalCockpitAddProfile = (_params, _options = {}) => {
    let nationalityCode = 'DEU'

    if (_params.form_data.applicant.nationality.hasOwnProperty('code')) {
        nationalityCode = _params.form_data.applicant.nationality.code
    }
    let codingLanguages = []
    for (let i = 0; i < _params.form_data.coding_languages.length; i++) {
        codingLanguages[i] = _params.form_data.coding_languages[i].text
    }
    let frameworks = []
    for (let i = 0; i < _params.form_data.frameworks.length; i++) {
        frameworks[i] = _params.form_data.frameworks[i].text
    }
    let englishLang = _params.form_data.proficiency.english_lang
    if (_params.form_data.proficiency.english_lang === 'very-good') {
        englishLang = 'very_good'
    }
    let germanLang = _params.form_data.proficiency.german_lang
    if (_params.form_data.proficiency.german_lang === 'very-good') {
        germanLang = 'very_good'
    }
    const data = {
        "ID": _params.form_data.external_key,
        "FinalAgreement": "accepted",
        "Age": _params.form_data.applicant.age,
        "FirstName": _params.form_data.applicant.first_name,
        "LastName": _params.form_data.applicant.last_name,
        "Email": _params.form_data.applicant.email,
        "JobSelections": _params.form_data.job_selection.join(","),
        "Phone": _params.form_data.applicant.phone,
        "NationalityCode": nationalityCode,
        "EnglishLanguageSkill": englishLang,
        "GermanLanguageSkill": germanLang,
        "CodingLanguages": codingLanguages.join(","),
        "Frameworks": frameworks.join(","),
        "Sectors": _params.form_data.platform_comfort_segment_1.join(","),
        "WorkTypes": _params.form_data.work_type.join(","),
        "GithubUrl": _params.form_data.profiles.github,
        "StackoverflowUrl": _params.form_data.profiles.stackoverflow,
        "LinkedinUrl": _params.form_data.profiles.linkedin,
        "XingUrl": _params.form_data.profiles.xing,
        "OtherUrl": _params.form_data.profiles.other_profile,
        "PortfolioUrl": ""
    }
    let responseData = {}
    let cp_options = {
        method: 'POST',
        url: config.allgeier.cockpit.appServerUrl + '/api/gpe/profile/AddProfile',
        headers: {
            'APPSERVER_AUTH_TOKEN': config.allgeier.cockpit.appServerAccessToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    // API call to Allgeier
    return rp(cp_options)
        .then(function (response) {
            // POST succeeded...
            // valid example {"response":{"endpoint":"Cockpit","payload":"113263502"}}
            // "external_key" is just a string so we put in external_key and also payload is empty then
            responseData = {
                response: {
                    endpoint: "Cockpit",
                    external_key: response,
                    status: 200,
                    payload: {}
                }
            }
            return [responseData, response]
        })
        .catch(function (error) {
            // POST failed...
            responseData = {
                response: {
                    endpoint: "Cockpit",
                    external_key: -1, // populate faulty user to process in frontend as "valid" but send info to admin
                    status: error.statusCode,
                    payload: JSON.parse(error.error)
                }
            }
            return [responseData, error]
        })
}


// Cockpit AddProfile
exports.externalCockpitUpdateProfile = (_params, _options = {}) => {
    let nationalityCode = 'DEU'

    if (_params.form_data.applicant.nationality.hasOwnProperty('code')) {
        nationalityCode = _params.form_data.applicant.nationality.code
    }
    let codingLanguages = []
    for (let i = 0; i < _params.form_data.coding_languages.length; i++) {
        codingLanguages[i] = _params.form_data.coding_languages[i].text
    }
    let frameworks = []
    for (let i = 0; i < _params.form_data.frameworks.length; i++) {
        frameworks[i] = _params.form_data.frameworks[i].text
    }
    let englishLang = _params.form_data.proficiency.english_lang
    if (_params.form_data.proficiency.english_lang === 'very-good') {
        englishLang = 'very_good'
    }
    let germanLang = _params.form_data.proficiency.german_lang
    if (_params.form_data.proficiency.german_lang === 'very-good') {
        germanLang = 'very_good'
    }
    const data = {
        "ID": _params.form_data.external_key,
        "FinalAgreement": "accepted",
        "Age": _params.form_data.applicant.age,
        "FirstName": _params.form_data.applicant.first_name,
        "LastName": _params.form_data.applicant.last_name,
        "Email": _params.form_data.applicant.email,
        "JobSelections": _params.form_data.job_selection.join(","),
        "Phone": _params.form_data.applicant.phone,
        "NationalityCode": nationalityCode,
        "EnglishLanguageSkill": englishLang,
        "GermanLanguageSkill": germanLang,
        "CodingLanguages": codingLanguages.join(","),
        "Frameworks": frameworks.join(","),
        "Sectors": _params.form_data.platform_comfort_segment_1.join(","),
        "WorkTypes": _params.form_data.work_type.join(","),
        "GithubUrl": _params.form_data.profiles.github,
        "StackoverflowUrl": _params.form_data.profiles.stackoverflow,
        "LinkedinUrl": _params.form_data.profiles.linkedin,
        "XingUrl": _params.form_data.profiles.xing,
        "OtherUrl": _params.form_data.profiles.other_profile,
        "PortfolioUrl": ""
    }
    let responseData = {}
    let cp_options = {
        method: 'POST',
        url: config.allgeier.cockpit.appServerUrl + '/api/gpe/profile/UpdateProfile',
        headers: {
            'APPSERVER_AUTH_TOKEN': config.allgeier.cockpit.appServerAccessToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    // API call to Allgeier
    return rp(cp_options)
    .then(function (response) {
        // POST succeeded...
        // external_key should be "true"
        responseData = {
            response: {
                endpoint: "Cockpit",
                external_key: _params.form_data.external_key, // pipe through
                status: 200,
                payload: response
            }
        }
        return [responseData, response]
    })
    .catch(function (error) {
        // POST failed...
        responseData = {
            response: {
                endpoint: "Cockpit",
                external_key: _params.form_data.external_key, // pipe through
                status: error.statusCode,
                payload: JSON.parse(error.error)
            }
        }
        return [responseData, error]
    })
}
