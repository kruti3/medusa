/**
 * Created by Jiri Smolik <jiri.smolik@ackee.cz> on 17.01.2017.
 */

const strategies = {
    and: {
        successFast: false,
        failFast: true,
    },
    or: {
        successFast: true,
        failFast: false,
    },
    series: {
        successFast: false,
        failFast: true,
    },
    all: {
        successFast: false,
        failFast: false,
    },
}

/**
 * @param {Promise[]} tasks
 * @param strategy
 */
module.exports = (tasks, _strategy) => {
    if (tasks && tasks.length < 1) {
        return Promise.resolve(true)
    }

    const s = strategies[_strategy] || strategies.series
    const next = (i, strategy) => {
        const task = tasks[i]
        const hasNext = !!tasks[i + 1]
        return task()
            .then(result => (!hasNext || strategy.successFast ? result : next(i + 1, strategy)))
            .catch(error => (!hasNext || strategy.failFast ? Promise.reject(error) : next(i + 1, strategy)))
    }
    return next(0, s)
}
