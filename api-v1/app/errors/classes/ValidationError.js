const JsonError = require('./JsonError')

/** Class representing Validation error (422) */
class ValidationError extends JsonError {
    /**
     * @param {String} message Error message
     * @param {Number} errorCode Custom error code
     */
    constructor(message, errorCode = 0) {
        super(message, 422, errorCode)
    }
}

module.exports = ValidationError
