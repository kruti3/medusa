const _ = require('lodash')
const BadRequestError = require('./classes/BadRequestError')
const enums = require('../enums')
const ForbiddenError = require('./classes/ForbiddenError')
const NotFoundError = require('./classes/NotFoundError')
const UnauthorizedError = require('./classes/UnauthorizedError')
const ValidationError = require('./classes/ValidationError')


exports.language = {
    notFound(languageId, message) {
        return exports.new.notFound(
            message || `Language #${languageId} not found.`,
            enums.errorCodes.language.languageNotFound
        )
    },
}

exports.new = {
    badRequest(message = 'Bad Request', code) {
        return new BadRequestError(message, code)
    },
    forbidden(message, code) {
        return new ForbiddenError(message, code)
    },
    notFound(message, code) {
        return new NotFoundError(message, code)
    },
    unauthorized(message, code) {
        return new UnauthorizedError(message, code)
    },
    validation(message, code) {
        return new ValidationError(message, code)
    },
}

exports.user = {
    duplicity(message = 'User with this email already exist') {
        const paramsStr = JSON.stringify({
            localizationKey: 'user.duplicity',
            origMessage: message,
            replacements: [],
        })

        return exports.new.validation(paramsStr, 5000)
    },
    disabled(message = 'This account is disabled') {
        return exports.new.forbidden(message, 4000)
    },
    tokenExpired(message = 'Token has expired or not valid') {
        return exports.new.unauthorized(message, 3000)
    },
    userDeleted(message = 'User associated to this token was deleted') {
        return exports.new.unauthorized(message, 3001)
    },
    userDisabled(message = 'Actual user is disabled') {
        return exports.new.unauthorized(message, 3002)
    },
    userDoesNotExist(message = 'User does not exist') {
        const paramsStr = JSON.stringify({
            localizationKey: 'user.userDoesNotExist',
            origMessage: message,
            replacements: [],
        })

        return exports.new.forbidden(paramsStr, 3003)
    },
    userSkillJobDuplicateEntry(message) {
        return exports.new.validation(
            message || 'Duplicate entry for key skills user_id job_id unique.',
            enums.errorCodes.user.userSkillJobDuplicateEntry
        )
    },
}

