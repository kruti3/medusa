const user = require('../controllers/api/userHR4You')
const express = require('express')

const router = express.Router() // eslint-disable-line new-cap

router.post('/add-profile', user.register)

module.exports = router
