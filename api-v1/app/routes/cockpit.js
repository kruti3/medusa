const user = require('../controllers/api/userCockpit')
const express = require('express')

const router = express.Router() // eslint-disable-line new-cap

//router.get('/get-token', user.createAccessToken)
router.post('/add-profile', user.register)
router.post('/update-profile', user.update)

module.exports = router
