//const _ = require('lodash')
const userService = require('../../services/userService')

/**
 *
 * @param {Req} req
 * @param {Res} res
 * @param {Next} next
 * @return {Promise.<T>|Promise}
 */
exports.register = (req, res, next) => {
    return userService.createCockpitUser(req.body)
    .then(user => {
        res.status(201)
        res.out = user
        return next()
    })
    .catch(next)
}

/**
 *
 * @param {Req} req
 * @param {UpdatableCockpitUser} req.body
 * @param {Res} res
 * @param {Next} next
 * @return {Promise|Promise.<T>|*}
 */
exports.update = (req, res, next) => {
    return userService.updateCockpitUser(req.body)
    .then(user => {
        res.status(201)
        res.out = user
        return next()
    }).catch(next)
}
