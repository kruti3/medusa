const _ = require('lodash')
const userService = require('../../services/userService')

/**
 *
 * @param {Req} req
 * @param {UpdatableUser} req.body
 * @param {Res} res
 * @param {Next} next
 * @return {Promise.<T>|Promise}
 */
exports.register = (req, res, next) => {
    return userService.createHR4YouUser(req.body, { locale: req.i18n.locale }).then(user => {
        res.status(201)
        res.out = user
        return next()
    }).catch(next)
}
