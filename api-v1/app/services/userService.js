const _ = require('lodash')
const helperLog = require('../components/helperLog')
const config = require('../../config/config')
const enums = require('../enums')
const validator = require('../components/validator')
const allgeierCockpit = require('../components/Allgeier/cockpit')
const mailer  = require('../components/SendGrid/sendgrid');

// HR4You
exports.createHR4YouUser = (_params, _options = {}) => {
    let validSubform = false
    const params = _.cloneDeep(_params)
    if (params.email) {
        params.email = _.toLower(params.email)
    }
    const options = _.merge(_options)
    options.user = { roleName: options.roleName, emailType: options.emailType }

    // get target endpoint name from form_data meta
    // see formConfig.json in aexpro-<name>/config/<env>/formConfig.json
    // "api": {
    //     "enabled": false,
    //         "endpoint": "hr4you",
    //         "version": "1.0.2",
    //         "short": "aexpro_form_berlin"
    // }

    let targetFormName = params.form_data.api.short

    // does targetFormName exists?
    if(config.aexpro_microsite[targetFormName]) {
        validSubform = true
    }

    let responseData = {
        response: {
            endpoint: "HR4You_SG",
            external_key: 0,
            status: 200,
            payload: {
                message: '',
                mtarget: '',
                mform: '',
                mstep: '',
                mtype: ''
            }
        }
    }

    // return validation check of form_data
    return validator.validate(params, validator.validations.hr4you_user.register)

    // process Allgeier HR4You
    // Mail To User
    .then(() => {
        if (validSubform && config.sendgrid.enabled === true) {
            return mailer.hr4youSendEmailToUser(params.form_data, 'aexpro_microsite', targetFormName, 'step1', 'response')
        }
        return responseData
    })
    // Mail To AEXPro
    .then(() => {
        if (validSubform && config.sendgrid.enabled === true) {
            return mailer.hr4youSendEmailToAdminWithData(params.form_data, 'aexpro_microsite', targetFormName, 'step1', 'datanotify')
        }
        return responseData
    })
    // Mail To Neo
    .then(() => {
        if (validSubform && config.sendgrid.enabled === true) {
            return mailer.hr4youSendEmailToAdminWithData(params.form_data, 'aexpro_microsite', targetFormName, 'step1', 'datanotify_neo')
        }
        return responseData
    })
}

/**
 * Register new user
 * @param {CreatableCockpitUser} _params
 * @param {Options=} _options
 * @param {!String=} _options.roleName if user should be created with new role
 * @returns {Promise.<UpdatableCockpitUser>}
 */
exports.createCockpitUser = (_params, _options = {}) => {
    const params = _.cloneDeep(_params)
    if (params.email) {
        params.email = _.toLower(params.email)
    }
    const options = _.merge(_options)
    options.user = { roleName: options.roleName, emailType: options.emailType }

    // return validation check of form_data
    return validator.validate(params, validator.validations.cockpit_user.register)

    // process Allgeier Cockpit
    .then(() => {
        // 1. try to send to Allgeier Cockpit
        return allgeierCockpit.externalCockpitAddProfile(params, options)
    })
    .then(([responseObj,rawData]) => {
        if (config.sendgrid.enabled === true) {
            let hasError = false
            if (responseObj.response.external_key === -1) {
                hasError = true
            }
            mailer.cockpitSendEmailToUser(params.form_data, 'step1', 'response', hasError)
        }
        return [responseObj,rawData]
    })
    .then(([responseObj,rawData]) => {
        if(config.sendgrid.enabled === true) {
            let hasError = false
            if (responseObj.response.external_key === -1) {
                hasError = true
            }
            mailer.cockpitSendEmailToAdminWithData(params.form_data, 'step1', 'datanotify', hasError, rawData) // send with error from Cockpit
        }
        return responseObj
    })
}

/**
 * Update new/existing user
 * @param {CreatableCockpitUser} _params
 * @param {Options=} _options
 * @param {!String=} _options.roleName if user should be created with new role
 * @returns {Promise.<UpdatableCockpitUser>}
 */
exports.updateCockpitUser = (_params, _options = {}) => {
    const params = _.cloneDeep(_params)
    if (params.email) {
        params.email = _.toLower(params.email)
    }
    const options = _.merge(_options)
    options.user = { roleName: options.roleName, emailType: options.emailType }

    // return validation check of form_data
    return validator.validate(params, validator.validations.cockpit_user.update)

    // process Allgeier Cockpit
    .then(() => {
        // 1. try to send to Allgeier Cockpit
        return allgeierCockpit.externalCockpitUpdateProfile(params, options)
    })
    .then(([responseObj,rawData]) => {
        if(config.sendgrid.enabled === true) {
            let hasError = false
            if (responseObj.response.external_key === -1) {
                hasError = true
            }
            mailer.cockpitSendEmailToUser(params.form_data, 'step2', 'response', hasError)
        }
        return [responseObj,rawData]
    })
    .then(([responseObj,rawData]) => {
        if(config.sendgrid.enabled === true) {
            let hasError = false
            if (responseObj.response.external_key === -1) {
                hasError = true
            }
            mailer.cockpitSendEmailToAdminWithData(params.form_data, 'step2', 'datanotify', hasError, rawData) // send with error from Cockpit
        }
        return responseObj // return only curated response, no Cockpit raw data (exposes API TOKEN or similar)
    })
}