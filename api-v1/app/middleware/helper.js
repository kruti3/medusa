const _ = require('lodash')

exports.createPaginationObject = (req, res, next) => {
    const pagination = _.pick(req.query, ['page', 'pageSize', 'limit', 'offset'])
    req.pagination = {}
    _.forEach(pagination, (val, key) => {
        req.pagination[key] = _.toInteger(val)
    })

    return next()
}

exports.transformMeToUserId = (req, res, next) => {
    if (req.params.userId === 'me') {
        req.userId = req.user.id
    } else {
        req.userId = req.params.userId
    }

    return next()
}

exports.returnBearerIfNotLoaded = bearer => (req, res, next) => {
    if (_.isObject(req.user)) {
        return next()
    }

    return bearer
}
