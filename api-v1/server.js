require('dotenv').config()
const express = require('express')
const passport = require('passport')
const config = require('./config/config')
const helperLog = require('./app/components/helperLog')
const _ = require('lodash')
const https = require('https')
var fs = require('fs')
const app = express()
const port = process.env.PORT || config.server.port
const initPromises = []
let appObj = null
let serverObj = null
if (config.params.email === true) {
    const email = require('./app/components/email') // eslint-disable-line global-require
    email.initialize(config.sendgrid.apiKey)
}

// Bootstrap application settings
require('./config/express')(app, passport)

// Bootstrap passport config
require('./config/passport')(passport, config)

// Bootstrap routes
require('./config/routes')(app, passport)

Promise.all(initPromises).then(() => {
    if (config.server.ssl === true) {
        const options = {
            key: fs.readFileSync(config.server.keyfile),
            cert: fs.readFileSync(config.server.certfile)
        };
        serverObj = https.createServer(options, app);
        appObj = serverObj.listen(port)
    } else {
        appObj = app.listen(port)
    }

    return helperLog.info({ params: config.params, port, processEnv: process.env }, 'Started app')
}).catch(err => helperLog.error({ err }, 'Starting server'))

exports.app = () => {
    helperLog.info('Request for running app')
    return new Promise(resolve => {
        const interval = setInterval(() => {
            helperLog.info('Checking if app is listening already')
            if (_.isObject(appObj)) {
                clearInterval(interval)
                return resolve(appObj)
            }

            return null
        }, 100)
    })
}
