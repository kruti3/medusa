var apiBenchmark = require('api-benchmark');
var fs = require('fs');

var service = {
	localhost: "http://localhost:3100/"
};

var routes = {
        default: {
            method: 'get',
            route: '/',
            headers: { 'Accept': 'application/json' }
        },
    healthcheck: {
        method: 'get',
        route: '/_health/666111',
        headers: { 'Accept': 'application/json' }
    },
		addprofilecockpit: {
			method: 'post',
			route: 'api/v1/cp/user/add-profile',
			headers: {
				'Accept': 'application/json'
			},
            data: {
                "form_data": {
                    "metadata": {
                        "locale": "en",
                        "uuid": "",
                        "token": "",
                        "created": 0,
                        "updated": 0,
                        "version": 1
                    },
                    "api": {
                        "lastmessage": "none",
                        "short": "aejobs_form",
                        "version": "1.0.2",
                        "endpoint": "cockpit"
                    },
                    "external_key": 0,
                    "applicant": {
                        "age": "44",
                        "first_name": "test",
                        "last_name": "999",
                        "email": "test@test.com",
                        "phone": "",
                        "nationality": {}
                    },
                    "job_selection": [
                        "administrator",
                        "project-manager"
                    ],
                    "mutual_respect": true,
                    "work_type": [],
                    "coding_languages": [],
                    "frameworks": [],
                    "proficiency": {
                        "english_lang": "",
                        "german_lang": "",
                        "business_travel": ""
                    },
                    "platform_comfort_segment_1": [],
                    "profiles": {
                        "github": "",
                        "stackoverflow": "",
                        "linkedin": "",
                        "xing": "",
                        "other_profile": ""
                    }
                }
            }
		}};

apiBenchmark.measure(service, routes, {
			debug: false,
			runMode: 'parallel',
			maxConcurrentRequests: 100,
			delay: 0,
			maxTime: 100000,
			minSamples: 1000,
			stopOnError: false
		}, function(err, results){
	apiBenchmark.getHtml(results, function(error, html){
		fs.writeFileSync('tests/benchmarks.html', html);
	});
});
