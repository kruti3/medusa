# README #

This README documents what steps are necessary to get the development environment up and running.

## What is this repository for? ##

* This repository holds all files for the development environment
* Version 1.0

## How do I get set up? ##

### Summary of set up ###

The development environment will be set up with Docker, Kubernetes and Make.

### Configuration ###

Please make sure that the following tools are installed on your local machine:

* [Make](http://ftp.gnu.org/gnu/make/) >= 3.81
(On macOS just install the command line developer tools in a terminal: `xcode-select --install`)
* [Docker](https://www.docker.com/community-edition#/download) >= 18.03.1-ce
* [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) >= v0.27.0 (If you have macOS with a hypervisor
like VirtualBox and the package manager Homebrew installed you can simply run in a terminal: `brew cask install minikube`)

To set the credentials to access the Docker container registry run the following command in a terminal at the root
directory of the repository and provide the fully qualified name of the credential files:

`. setEnv.sh`

### Build & Deployment instructions ###

#### Working with Minikube ####

To be able to work with the Docker daemon from Minikube on your local machine use the command `docker-env` in your
terminal after starting the VM:

1. `minikube start`
2. `eval $(minikube docker-env)`

Now you can use Docker in the terminal on your local machine talking to the Docker daemon inside the Minikube VM:

`docker images -a`

HINT: You can undo this change by executing:

`eval $(minikube docker-env -u)`

#### Deploy to Kubernetes cluster ####

To deploy the Docker container to the Minikube single-node Kubernetes cluster inside the VM on your local machine
run the following command in a terminal at the root directory of the repository:

`make`

After a little while you can open the service:

`minikube service api`

You can remove the resources from the Kubernetes cluster and stop it afterwards by executing the following commands:

1. `make clean`
2. `eval $(minikube docker-env -u)`
3. `minikube stop`
