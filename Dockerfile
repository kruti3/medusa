FROM atyoucastaejobs.azurecr.io/node-base:1.0.2

ARG NODE_ENV
ENV NODE_ENV ${NODE_ENV}

COPY api-v1/package.json .
COPY api-v1/package-lock.json .

RUN npm install

COPY api-v1 .

USER root

CMD ["npm", "run", "start" ]
