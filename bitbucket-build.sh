#!/usr/bin/env bash
NODE_ENV=$1

# Set $DOCKER_IMAGE from environment variables in repository settings
DOCKER_IMAGE=${DOCKER_REGISTRY}/${DOCKER_IMAGE_NAME}
# Authenticate with the Docker registry
echo ${DOCKER_PASSWORD_BUILD} > ./docker-password
cat ./docker-password | docker login --username ${DOCKER_USERNAME_BUILD} --password-stdin ${DOCKER_REGISTRY}
# Build the Docker image
docker build --build-arg NODE_ENV=${NODE_ENV} -t ${DOCKER_IMAGE}:${BITBUCKET_COMMIT} .
# Push the Docker image to the Docker registry
docker push ${DOCKER_IMAGE}:${BITBUCKET_COMMIT}
# Logout from Docker registry
docker logout ${DOCKER_REGISTRY}
