const sgMail = require('@sendgrid/mail');
const path = require('path');
const config = require(path.join(__dirname, '../../config/sendgrid.'+process.env.NODE_ENV+'.json'));
const templatepath = path.join(__dirname, '../../templates/sendgrid/');
const fs = require('fs');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

function getDateTimeAsString() {
    return (new Date()).toISOString().substring(0, 10) + "_" + (new Date().getHours()) + (new Date().getUTCMinutes()) + (new Date().getUTCSeconds())
}

function loadTemplateToString(fname,locale) {
    const filename  = path.join(templatepath + locale + "/" + fname);
    //console.log("loadTemplateToString: filename -> " + filename)
    var mycontent = ''
    if (fs.existsSync(filename)) {
        mycontent = fs.readFileSync(filename, "utf8");
    } else {
        console.log("FATAL: file not found -> " + filename)
    }
    return mycontent
}

// sendEmailToUser
function sendEmailToUser(payload,mstep,mtype) {
    if (mstep === "none") return
    // load template
    const locale = payload.metadata.locale
    const template = loadTemplateToString(config.aejobs_form[mstep][mtype]['template'][locale],locale)
    const msg = {
        to: payload.applicant.email,
        from: config.aejobs_form[mstep][mtype].mail_from,
        subject: config.aejobs_form[mstep][mtype].mail_subject[locale],
        html: template,
        substitutionWrappers: ['<%', '%>'],
        substitutions: {
            first_name: payload.applicant.first_name,
            last_name: payload.applicant.last_name,
        },
    };
    sgMail
    .send(msg)
    //.then(() => console.log('Mail sent successfully: '+mstep+' - '+mtype))
    .catch(error => console.error(error.toString()));
}

// sendEmailToAdminWithData
function sendEmailToAdminWithData(payload,mstep,mtype) {
    if (mstep === "none") return
    // encode attachment (if any)
    // assume payload is JSON
    const s   = JSON.stringify(payload)
    const buf = Buffer.from(s.toString(), 'ascii');
    const attachment = buf.toString('base64');
    const msg = {
        to: config.aejobs_form[mstep][mtype].mail_to,
        from: config.aejobs_form[mstep][mtype].mail_from,
        subject: config.aejobs_form[mstep][mtype].mail_subject,
        text: config.aejobs_form[mstep][mtype].text,
        html: config.aejobs_form[mstep][mtype].html,
        // use extern_id or other for filename
        attachments: [
            {
                content: attachment,
                filename: getDateTimeAsString() + '.json.txt',
            },
        ],
    };
    sgMail
    .send(msg)
    //.then(() => console.log('Mail sent successfully: '+mstep+' - '+mtype))
    .catch(error => console.error(error.toString()));

}

// exports
module.exports.mailConfig = config;
module.exports.loadTemplateToString = loadTemplateToString;
module.exports.sendEmailToUser = sendEmailToUser;
module.exports.sendEmailToAdminWithData = sendEmailToAdminWithData;
