const express = require('express');
const users = require('../services/users');

const router = new express.Router();

const request = require('request');
const mailer  = require('../../lib/sendgrid');
var   status  = "undefined"
var   step    = "none"
/**
 *
 */
router.get('/', async (req, res, next) => {
  if (process.env.NODE_ENV === 'development') {
    const options = {};

    try {
      const result = await users.getUsers(options);
      res.status(result.status || 200).send(result.data);
    } catch (err) {
      return res.status(500).send({
        status: 500,
        error: 'Server Error'
      });
    }
  } else if (process.env.NODE_ENV === 'staging') {
    request({
      method: 'GET',
      url: process.env.ALLGEIER_APPSERVER_URL + '/api/gpe/common/GetProfile/' + req.query.id_profile,
      headers: {
        'APPSERVER_AUTH_TOKEN': process.env.ALLGEIER_APPSERVER_TOKEN,
        'Content-Type': 'application/json'
      }
    }).pipe(res);
  } else {
    return res.status(404).send({
      status: 404,
      error: 'Not found'
    });
  }
});


/**
 *
 */
router.post('/', function (req, res, next) {
  if (process.env.NODE_ENV === 'development') {
    const result = {
      status: 200,
      data: 'postUsers ok!'
    };
    // handle OPTION
    if (req.method === 'POST') {
        if(process.env.SENDGRID_API_ENABLE === 'true') {
            if (req.body.form_data.external_key === 0) {
                step = "step1"
            } else {
                step = "step2"
            }
            console.log("processing devmode: step="+step);
            if (result.status === 200) {
                mailer.sendEmailToUser(req.body.form_data,step,'response');
                mailer.sendEmailToAdminWithData(req.body.form_data,step,'datanotify');
            }
        }
    } else {
      console.log("ignoring req.method="+req.method)
    }
    res.status(result.status || 200).send(result.data);
  } else {
    let nationalityCode = 'DEU';
    if (req.body.form_data.applicant.nationality.hasOwnProperty('code')) {
      nationalityCode = req.body.form_data.applicant.nationality.code;
    }
    let codingLanguages = [];
    for (let i = 0; i < req.body.form_data.coding_languages.length; i++) {
      codingLanguages[i] = req.body.form_data.coding_languages[i].text;
    }
    let frameworks = [];
    for (let i = 0; i < req.body.form_data.frameworks.length; i++) {
      frameworks[i] = req.body.form_data.frameworks[i].text;
    }
    let englishLang = req.body.form_data.proficiency.english_lang;
    if (req.body.form_data.proficiency.english_lang === 'very-good') {
      englishLang = 'very_good';
    }
    let germanLang = req.body.form_data.proficiency.german_lang;
    if (req.body.form_data.proficiency.german_lang === 'very-good') {
      germanLang = 'very_good';
    }
    const data = {
      "ID": req.body.form_data.external_key,
      "FinalAgreement": "accepted",
      "Age": req.body.form_data.applicant.age,
      "FirstName": req.body.form_data.applicant.first_name,
      "LastName": req.body.form_data.applicant.last_name,
      "Email": req.body.form_data.applicant.email,
      "JobSelections": req.body.form_data.job_selection.join(","),
      "Phone": req.body.form_data.applicant.phone,
      "NationalityCode": nationalityCode,
      "EnglishLanguageSkill": englishLang,
      "GermanLanguageSkill": germanLang,
      "CodingLanguages": codingLanguages.join(","),
      "Frameworks": frameworks.join(","),
      "Sectors": req.body.form_data.platform_comfort_segment_1.join(","),
      "WorkTypes": req.body.form_data.work_type.join(","),
      "GithubUrl": req.body.form_data.profiles.github,
      "StackoverflowUrl": req.body.form_data.profiles.stackoverflow,
      "LinkedinUrl": req.body.form_data.profiles.linkedin,
      "XingUrl": req.body.form_data.profiles.xing,
      "OtherUrl": req.body.form_data.profiles.other_profile,
      "PortfolioUrl": ""
    };
    let url = process.env.ALLGEIER_APPSERVER_URL + '/api/gpe/profile/';
    if (req.body.form_data.external_key === 0) {
      url = url + 'AddProfile';
      step = "step1"
    } else {
      url = url + 'UpdateProfile';
      step = "step2"
    }

    // API call to Allgeier
    request({
      method: 'POST',
      url: url,
      headers: {
          'APPSERVER_AUTH_TOKEN': process.env.ALLGEIER_APPSERVER_TOKEN,
          'Content-Type': 'application/json'
        },
      body: JSON.stringify(data)
      },
      function (error, response, body) {
          if (!error && response.statusCode === 200) {
              status = "success"
          } else {
              status = "error"
          }
      }
    ).pipe(res)

    // send mail to USER
    if (status === "success") {
      mailer.sendEmailToUser(req.body.form_data,step,'response');
      mailer.sendEmailToAdminWithData(req.body.form_data,step,'datanotify');
    }
    // else {
    //  console.log("allgeier api call seems to be a failure ...")
    //}
  }
});

module.exports = router;
