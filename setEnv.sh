#!/bin/bash

PROFILE=~/.bash_profile

if [ -z ${ACR_ID_FILE} ]; then
    acr_id_file=""
    until [[ -f ${acr_id_file} ]]; do
        echo "Enter the fully qualified name to Azure Container Registry ID file (e.g. /path/id_file): "
        read acr_id_file
    done
    echo "export ACR_ID_FILE=$acr_id_file" >> ${PROFILE}
fi

if [ -z ${ACR_PW_FILE} ]; then
    acr_pw_file=""
    until [[ -f ${acr_pw_file} ]]; do
        echo "Enter the fully qualified name to Azure Container Registry password file (e.g. /path/pw_file): "
        read acr_pw_file
    done
    echo "export ACR_PW_FILE=$acr_pw_file" >> ${PROFILE}
fi

. ${PROFILE}
